package pl.piraszewski.poccofinance.demo.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TermDaoTest {

    private TermDao termDao;

    @Before
    public void before() {
        termDao = new TermDao();
    }

    @Test
    public void termTest() {
        termDao.setMaxTerm(100L);
        Assert.assertEquals(new Long(100L), termDao.getMaxTerm());
    }

    @Test
    public void maxExtendTermTest() {
        termDao.setMaxExtendTerm(100L);
        Assert.assertEquals(new Long(100L), termDao.getMaxExtendTerm());
    }


}