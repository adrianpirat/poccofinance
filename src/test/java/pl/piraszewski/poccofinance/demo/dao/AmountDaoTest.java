package pl.piraszewski.poccofinance.demo.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class AmountDaoTest {

    private AmountDao amountDao;

    @Before
    public void before() {
        amountDao = new AmountDao();
    }

    @Test
    public void minTest() {
        amountDao.setMin(new BigDecimal(1.2));
        Assert.assertEquals(new BigDecimal(1.2), amountDao.getMin());
    }

    @Test
    public void maxTest() {
        amountDao.setMax(new BigDecimal(10.5));
        Assert.assertEquals(new BigDecimal(10.5), amountDao.getMax());
    }

}