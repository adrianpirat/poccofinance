package pl.piraszewski.poccofinance.demo.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.piraszewski.poccofinance.demo.api.model.ErrorEnum;
import pl.piraszewski.poccofinance.demo.dao.TermDao;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;

import static org.mockito.Mockito.when;

public class TermValidatorTest {

    private TermValidator termValidator;

    @Mock
    private TermDao termDao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        termValidator = new TermValidator(termDao);
        when(termDao.getMaxTerm()).thenReturn(100L);
        when(termDao.getMaxExtendTerm()).thenReturn(100L);
    }

    @Test
    public void termMaxTest() throws BusinessValidationException {
        termValidator.validator(100L);
    }

    @Test
    public void extendDaysTest() throws BusinessValidationException {
        termValidator.validateExtendDays(10L, 10L);
    }

    @Test
    public void extendDaysErrorTest() {
        try {
            termValidator.validateExtendDays(10L, 1000L);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.TIME_AMOUNT.getMsg());
        }
    }

    @Test
    public void extendDaysNullTest() {
        try {
            termValidator.validateExtendDays(null, null);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.EXTEND_VALUE.getMsg());
        }
    }

    @Test
    public void extendDaysZeroTest() {
        try {
            termValidator.validateExtendDays(0l, 0l);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.TIME_AMOUNT.getMsg());
        }
    }

    @Test
    public void termZeroTest() {
        try {
            termValidator.validator(0L);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.TIME_AMOUNT.getMsg());
        }
    }

    @Test
    public void termNullTest() {
        try {
            termValidator.validator(null);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.TIME_AMOUNT.getMsg());
        }
    }
}