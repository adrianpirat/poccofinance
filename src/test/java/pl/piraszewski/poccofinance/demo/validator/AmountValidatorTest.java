package pl.piraszewski.poccofinance.demo.validator;

import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.piraszewski.poccofinance.demo.api.model.ErrorEnum;
import pl.piraszewski.poccofinance.demo.dao.AmountDao;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;


public class AmountValidatorTest {

    private AmountValidator amountValidator;

    private static long TIME = 1539587340000l;

    @Mock
    private AmountDao amountDao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        amountValidator = new AmountValidator(amountDao);
        when(amountDao.getMax()).thenReturn(BigDecimal.valueOf(1000L));
        when(amountDao.getMin()).thenReturn(BigDecimal.valueOf(1L));
        DateTimeUtils.setCurrentMillisFixed(100L);
    }


    @After
    public void setAfter() {
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void AmountValidatorTest() throws BusinessValidationException {
        amountValidator.validator(BigDecimal.valueOf(100L), BigDecimal.TEN);
    }

    @Test
    public void AmountValidatorMinTest() {
        try {
            amountValidator.validator(BigDecimal.valueOf(-100L), BigDecimal.TEN);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.MIN_AMOUNT.getMsg());
        }
    }

    @Test
    public void AmountValidatorZeroTest() {
        try {
            amountValidator.validator(BigDecimal.valueOf(0), BigDecimal.TEN);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.MIN_AMOUNT.getMsg());
        }
    }

    @Test
    public void AmountValidatorOneTest() {
        try {
            amountValidator.validator(BigDecimal.valueOf(1), BigDecimal.TEN);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.MIN_AMOUNT.getMsg());
        }
    }

    @Test
    public void AmountValidatorTwoTest() throws BusinessValidationException {
        amountValidator.validator(BigDecimal.valueOf(2), BigDecimal.TEN);
    }

    @Test
    public void AmountValidatorMaksTest() {

        try {
            amountValidator.validator(BigDecimal.valueOf(5000000l), BigDecimal.TEN);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.MAX_AMOUNT.getMsg());
        }
    }

    @Test
    public void AmountValidatorMaxValueAndTimeTest() {
        try {
            amountValidator.validator(BigDecimal.valueOf(1000L), BigDecimal.TEN);
            Assert.fail();
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.TIME_AMOUNT.getMsg());
        }
    }

    @Test
    public void AmountValidatorMaxValueAndTimeAfterSixTest() throws BusinessValidationException {
        DateTimeUtils.setCurrentMillisFixed(TIME);
        amountValidator.validator(BigDecimal.valueOf(1000L), BigDecimal.TEN);
    }

    @Test
    public void AmountValidatorInterestTest() {
        try {
            DateTimeUtils.setCurrentMillisFixed(TIME);
            amountValidator.validator(BigDecimal.valueOf(40L), BigDecimal.ONE);
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.VALUE_INTEREST.getMsg());
        }
    }

    @Test
    public void AmountValidatorInterestNullTest() {
        try {
            DateTimeUtils.setCurrentMillisFixed(TIME);
            amountValidator.validator(BigDecimal.valueOf(10L), null);
        } catch (BusinessValidationException e) {
            Assert.assertEquals(e.getMessage(), ErrorEnum.NULL_INTEREST.getMsg());
        }
    }

}