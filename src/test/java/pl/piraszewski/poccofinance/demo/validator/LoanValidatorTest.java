package pl.piraszewski.poccofinance.demo.validator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.piraszewski.poccofinance.demo.api.model.LoanExtendRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanRequest;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;

import static org.junit.Assert.*;

public class LoanValidatorTest {

    private LoanValidator loanValidator;

    @Mock
    private AmountValidator amountValidator;

    @Mock
    private TermValidator termValidator;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        loanValidator = new LoanValidator(amountValidator, termValidator);
    }

    @Test
    public void loadValidatorTest() throws BusinessValidationException {
        LoanRequest loanRequest = new LoanRequest();
        loanValidator.validate(loanRequest);
    }

    @Test
    public void loadValidatorNullTest() {
        try {
            loanValidator.validate(null);
            Assert.fail();
        } catch (BusinessValidationException e) {

        }
    }

    @Test
    public void validateExtendTest() throws BusinessValidationException {
        LoanExtendRequest request = new LoanExtendRequest();
        loanValidator.validateExtend(request);
    }

    @Test
    public void validateExtendNullTest() {
        try {
            loanValidator.validateExtend(null);
            Assert.fail();
        } catch (BusinessValidationException e) {

        }
    }
}