package pl.piraszewski.poccofinance.demo.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import pl.piraszewski.poccofinance.demo.DemoApplication;
import pl.piraszewski.poccofinance.demo.api.model.LoanExtendRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanResponse;
import pl.piraszewski.poccofinance.demo.api.model.StatusEnum;

import java.math.BigDecimal;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = MOCK, classes = DemoApplication.class)
@AutoConfigureMockMvc
public class LoanControllerTestIT {

    @Autowired
    private MockMvc mvc;

    private ObjectMapper mapper;

    private static long TIME = 1539587340000l;

    @Before
    public void setUp() {
        mapper = new ObjectMapper();
        DateTimeUtils.setCurrentMillisFixed(TIME);
    }

    @After
    public void setAfter() {
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void notFoundtest() throws Exception {
        mvc.perform(get("/loan/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void applyForLoanDeclinedTest() throws Exception {
        LoanRequest loanRequest = new LoanRequest();
        String json = mapper.writeValueAsString(loanRequest);
        MvcResult mvcResult = mvc.perform(post("/loan/applyForLoan/")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable()).andReturn();
        String asyncResult = mvcResult.getResponse().getContentAsString();
        LoanResponse response = mapper.readValue(asyncResult, LoanResponse.class);
        Assert.assertEquals(StatusEnum.DECLINED, response.getStatusEnum());
    }

    @Test
    public void applyForLoanTest() throws Exception {
        LoanRequest loanRequest = new LoanRequest();
        loanRequest.setLoan(new BigDecimal(50));
        loanRequest.setLoanDays(50L);
        loanRequest.setInterest(BigDecimal.TEN);
        String json = mapper.writeValueAsString(loanRequest);
        MvcResult mvcResult = mvc.perform(post("/loan/applyForLoan/")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted()).andReturn();
        String asyncResult = mvcResult.getResponse().getContentAsString();
        LoanResponse response = mapper.readValue(asyncResult, LoanResponse.class);
        Assert.assertEquals(StatusEnum.ACCEPTED, response.getStatusEnum());
    }

    @Test
    public void extendLoanTest() throws Exception {
        LoanExtendRequest loanExtendRequest = new LoanExtendRequest();
        loanExtendRequest.setLoan(new BigDecimal(50));
        loanExtendRequest.setLoanDays(50L);
        loanExtendRequest.setExtendDays(30L);
        String json = mapper.writeValueAsString(loanExtendRequest);
        MvcResult mvcResult = mvc.perform(post("/loan/extendLoan/")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted()).andReturn();
        String asyncResult = mvcResult.getResponse().getContentAsString();
        LoanResponse response = mapper.readValue(asyncResult, LoanResponse.class);
        Assert.assertEquals(StatusEnum.ACCEPTED, response.getStatusEnum());
    }

    @Test
    public void extendLoanDeclinedTest() throws Exception {
        LoanExtendRequest loanExtendRequest = new LoanExtendRequest();
        loanExtendRequest.setLoan(new BigDecimal(50));
        loanExtendRequest.setLoanDays(50L);
        loanExtendRequest.setExtendDays(300000L);
        String json = mapper.writeValueAsString(loanExtendRequest);
        MvcResult mvcResult = mvc.perform(post("/loan/extendLoan/")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable()).andReturn();
        String asyncResult = mvcResult.getResponse().getContentAsString();
        LoanResponse response = mapper.readValue(asyncResult, LoanResponse.class);
        Assert.assertEquals(StatusEnum.DECLINED, response.getStatusEnum());
    }
}