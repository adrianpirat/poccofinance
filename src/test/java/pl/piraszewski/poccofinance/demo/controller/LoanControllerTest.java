package pl.piraszewski.poccofinance.demo.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pl.piraszewski.poccofinance.demo.api.model.LoanExtendRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanResponse;
import pl.piraszewski.poccofinance.demo.api.model.StatusEnum;
import pl.piraszewski.poccofinance.demo.service.LoanService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class LoanControllerTest {

    private LoanController loanController;

    @Mock
    private LoanService loanService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        loanController = new LoanController(loanService);
    }

    @Test
    public void loanTest() {
        LoanRequest loanRequest = new LoanRequest();
        LoanResponse loanResponse = new LoanResponse();
        loanResponse.setStatusEnum(StatusEnum.ACCEPTED);
        when(loanService.applyForLoan(any())).thenReturn(loanResponse);
        ResponseEntity responseEntity = loanController.applyForLoan(loanRequest);
        Assert.assertNotNull(responseEntity);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.ACCEPTED);
        Assert.assertEquals(((LoanResponse )responseEntity.getBody()).getStatusEnum(), StatusEnum.ACCEPTED);

    }

    @Test
    public void loanDeclinedTest() {
        LoanRequest loanRequest = new LoanRequest();
        LoanResponse loanResponse = new LoanResponse();
        loanResponse.setStatusEnum(StatusEnum.DECLINED);
        loanResponse.setReasonForRejection("Test");
        when(loanService.applyForLoan(any())).thenReturn(loanResponse);
        ResponseEntity responseEntity = loanController.applyForLoan(loanRequest);
        Assert.assertNotNull(responseEntity);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_ACCEPTABLE);
        Assert.assertEquals(((LoanResponse )responseEntity.getBody()).getStatusEnum(), StatusEnum.DECLINED);
        Assert.assertEquals(((LoanResponse )responseEntity.getBody()).getReasonForRejection(), loanResponse.getReasonForRejection());

    }

    @Test
    public void loanExtendDeclinedTest() {
        LoanExtendRequest loanRequest = new LoanExtendRequest();
        LoanResponse loanResponse = new LoanResponse();
        loanResponse.setStatusEnum(StatusEnum.DECLINED);
        loanResponse.setReasonForRejection("Test");
        when(loanService.extendLoan(any())).thenReturn(loanResponse);
        ResponseEntity responseEntity = loanController.extendLoan(loanRequest);
        Assert.assertNotNull(responseEntity);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_ACCEPTABLE);
        Assert.assertEquals(((LoanResponse )responseEntity.getBody()).getStatusEnum(), StatusEnum.DECLINED);
        Assert.assertEquals(((LoanResponse )responseEntity.getBody()).getReasonForRejection(), loanResponse.getReasonForRejection());
    }

    @Test
    public void loanExtendTest() {
        LoanExtendRequest loanRequest = new LoanExtendRequest();
        LoanResponse loanResponse = new LoanResponse();
        loanResponse.setStatusEnum(StatusEnum.ACCEPTED);
        when(loanService.extendLoan(any())).thenReturn(loanResponse);
        ResponseEntity responseEntity = loanController.extendLoan(loanRequest);
        Assert.assertNotNull(responseEntity);
        Assert.assertEquals(responseEntity.getStatusCode(), HttpStatus.ACCEPTED);
        Assert.assertEquals(((LoanResponse )responseEntity.getBody()).getStatusEnum(), StatusEnum.ACCEPTED);
    }


}
