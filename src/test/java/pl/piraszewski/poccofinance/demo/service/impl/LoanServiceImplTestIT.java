package pl.piraszewski.poccofinance.demo.service.impl;

import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.piraszewski.poccofinance.demo.api.model.LoanExtendRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanResponse;
import pl.piraszewski.poccofinance.demo.api.model.StatusEnum;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoanServiceImplTestIT {

    @Autowired
    private LoanServiceImpl loanService;

    private static long TIME = 1539587340000l;

    @Before
    public void setUp() {
        DateTimeUtils.setCurrentMillisFixed(TIME);
    }

    @After
    public void setAfter() {
        DateTimeUtils.setCurrentMillisSystem();
    }


    @Test
    public void loadServiceTest() {
        LoanRequest loanRequest = new LoanRequest();
        loanRequest.setLoan(BigDecimal.valueOf(100L));
        loanRequest.setLoanDays(100L);
        loanRequest.setInterest(BigDecimal.TEN);
        LoanResponse response = loanService.applyForLoan(loanRequest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getStatusEnum(), StatusEnum.ACCEPTED);
    }

    @Test
    public void loadExtendServiceTest() {
        LoanExtendRequest loanRequest = new LoanExtendRequest();
        loanRequest.setLoan(BigDecimal.valueOf(100L));
        loanRequest.setLoanDays(10L);
        loanRequest.setExtendDays(10L);
        loanRequest.setInterest(BigDecimal.TEN);
        LoanResponse response = loanService.extendLoan(loanRequest);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getStatusEnum(), StatusEnum.ACCEPTED);
    }
}