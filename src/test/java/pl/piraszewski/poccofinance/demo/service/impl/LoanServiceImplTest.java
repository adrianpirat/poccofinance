package pl.piraszewski.poccofinance.demo.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import pl.piraszewski.poccofinance.demo.api.model.*;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;
import pl.piraszewski.poccofinance.demo.service.LoanService;
import pl.piraszewski.poccofinance.demo.validator.LoanValidator;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

public class LoanServiceImplTest {

    private LoanService loanService;

    @Mock
    private LoanValidator loanValidator;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        loanService = new LoanServiceImpl( loanValidator);
    }

    @Test
    public void loanTest() {
        LoanRequest loanRequest = new LoanRequest();
        LoanResponse loanResponse = loanService.applyForLoan(loanRequest);
        Assert.assertNotNull(loanResponse);
        Assert.assertEquals(loanResponse.getStatusEnum(), StatusEnum.ACCEPTED);
    }

    @Test
    public void loanRejectTest() throws Exception {
        LoanRequest loanRequest = new LoanRequest();
        doThrow(new BusinessValidationException(ErrorEnum.EMPTY_LOAN)).when(loanValidator).validate(any());
        LoanResponse loanResponse = loanService.applyForLoan(loanRequest);
        Assert.assertNotNull(loanResponse);
        Assert.assertEquals(loanResponse.getStatusEnum(), StatusEnum.DECLINED);
        Assert.assertEquals(loanResponse.getReasonForRejection(), ErrorEnum.EMPTY_LOAN.getMsg());
    }

    @Test
    public void loanExtenedTest() {
        LoanExtendRequest loanRequest = new LoanExtendRequest();
        LoanResponse loanResponse = loanService.extendLoan(loanRequest);
        Assert.assertNotNull(loanResponse);
        Assert.assertEquals(loanResponse.getStatusEnum(), StatusEnum.ACCEPTED);
    }

    @Test
    public void loanExtenedRejectTest() throws Exception {
        LoanExtendRequest loanRequest = new LoanExtendRequest();
        doThrow(new BusinessValidationException(ErrorEnum.EMPTY_LOAN)).when(loanValidator).validateExtend(any());
        LoanResponse loanResponse = loanService.extendLoan(loanRequest);
        Assert.assertNotNull(loanResponse);
        Assert.assertEquals(loanResponse.getStatusEnum(), StatusEnum.DECLINED);
        Assert.assertEquals(loanResponse.getReasonForRejection(), ErrorEnum.EMPTY_LOAN.getMsg());
    }
}