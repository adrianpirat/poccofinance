package pl.piraszewski.poccofinance.demo.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.piraszewski.poccofinance.demo.api.model.LoanExtendRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanResponse;
import pl.piraszewski.poccofinance.demo.api.model.StatusEnum;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;
import pl.piraszewski.poccofinance.demo.service.LoanService;

@RestController
@RequestMapping("/loan")
public class LoanController {

    private LoanService loanService;

    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    @PostMapping("/applyForLoan/")
    public ResponseEntity applyForLoan(@RequestBody LoanRequest loanRequest) {
        LoanResponse response = loanService.applyForLoan(loanRequest);
        if (response.getStatusEnum() == StatusEnum.ACCEPTED) {
            return ResponseEntity.accepted().contentType(MediaType.APPLICATION_JSON_UTF8).body(response);
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
    }


    @PostMapping("/extendLoan/")
    public ResponseEntity extendLoan(@RequestBody LoanExtendRequest request) {
        LoanResponse response = loanService.extendLoan(request);
        if (response.getStatusEnum() == StatusEnum.ACCEPTED) {
            return ResponseEntity.accepted().contentType(MediaType.APPLICATION_JSON_UTF8).body(response);
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(response);
    }
}
