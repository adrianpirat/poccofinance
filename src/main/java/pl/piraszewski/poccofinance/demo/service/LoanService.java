package pl.piraszewski.poccofinance.demo.service;

import pl.piraszewski.poccofinance.demo.api.model.LoanExtendRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanResponse;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;

public interface LoanService {
    LoanResponse applyForLoan(LoanRequest loanRequest);

    LoanResponse extendLoan(LoanExtendRequest request);
}
