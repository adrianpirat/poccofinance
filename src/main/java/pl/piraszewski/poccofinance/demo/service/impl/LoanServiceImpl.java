package pl.piraszewski.poccofinance.demo.service.impl;

import org.springframework.stereotype.Service;
import pl.piraszewski.poccofinance.demo.api.model.LoanExtendRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanResponse;
import pl.piraszewski.poccofinance.demo.api.model.StatusEnum;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;
import pl.piraszewski.poccofinance.demo.service.LoanService;
import pl.piraszewski.poccofinance.demo.validator.LoanValidator;

@Service()
public class LoanServiceImpl implements LoanService {

    private LoanValidator loanValidator;

    public LoanServiceImpl( LoanValidator loanValidator) {

        this.loanValidator = loanValidator;
    }

    @Override
    public LoanResponse applyForLoan(LoanRequest loanRequest) {
        LoanResponse response = new LoanResponse();
        try {
            loanValidator.validate(loanRequest);
            response.setStatusEnum(StatusEnum.ACCEPTED);
        } catch (BusinessValidationException e) {
            response.setStatusEnum(StatusEnum.DECLINED);
            response.setReasonForRejection(e.getMessage());
        }
        return response;
    }

    @Override
    public LoanResponse extendLoan(LoanExtendRequest request) {
        LoanResponse response = new LoanResponse();
        try {
            loanValidator.validateExtend(request);
            response.setStatusEnum(StatusEnum.ACCEPTED);
        } catch (BusinessValidationException e) {
            response.setStatusEnum(StatusEnum.DECLINED);
            response.setReasonForRejection(e.getMessage());
        }
        return response;
    }
}
