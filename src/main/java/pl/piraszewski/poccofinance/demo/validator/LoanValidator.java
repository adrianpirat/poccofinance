package pl.piraszewski.poccofinance.demo.validator;

import org.springframework.stereotype.Component;
import pl.piraszewski.poccofinance.demo.api.model.ErrorEnum;
import pl.piraszewski.poccofinance.demo.api.model.LoanExtendRequest;
import pl.piraszewski.poccofinance.demo.api.model.LoanRequest;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;

@Component
public class LoanValidator {

    private AmountValidator amountValidator;

    private TermValidator termValidator;

    public LoanValidator(AmountValidator amountValidator, TermValidator termValidator) {
        this.amountValidator = amountValidator;
        this.termValidator = termValidator;
    }

    public void validate(LoanRequest loanRequest) throws BusinessValidationException {
        if (loanRequest == null) {
            throw new BusinessValidationException(ErrorEnum.NULL_LOAN_REQUEST);
        }
        amountValidator.validator(loanRequest.getLoan(), loanRequest.getInterest());
        termValidator.validator(loanRequest.getLoanDays());
    }

    public void validateExtend(LoanExtendRequest loanExtendRequest) throws BusinessValidationException {
        if (loanExtendRequest == null) {
            throw new BusinessValidationException(ErrorEnum.NULL_LOAN_REQUEST);
        }
        termValidator.validateExtendDays(loanExtendRequest.getLoanDays(), loanExtendRequest.getExtendDays());
    }
}
