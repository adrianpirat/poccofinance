package pl.piraszewski.poccofinance.demo.validator;

import org.springframework.stereotype.Component;
import pl.piraszewski.poccofinance.demo.api.model.ErrorEnum;
import pl.piraszewski.poccofinance.demo.dao.TermDao;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;

@Component
public class TermValidator {

    private TermDao termDao;

    public TermValidator(TermDao termDao) {
        this.termDao = termDao;
    }

    public void validator(Long time) throws BusinessValidationException {
        if (time == null || time <= 0 || time > termDao.getMaxTerm()) {
            throw new BusinessValidationException(ErrorEnum.TIME_AMOUNT);
        }
    }

    public void validateExtendDays(Long loanDays, Long extendDays) throws BusinessValidationException {
        if (loanDays == null | extendDays == null) {
            throw new BusinessValidationException(ErrorEnum.EXTEND_VALUE);
        }
        if (loanDays <= 0 || extendDays <= 0 || loanDays > termDao.getMaxTerm()) {
            throw new BusinessValidationException(ErrorEnum.TIME_AMOUNT);
        }
        if ((loanDays + extendDays) > termDao.getMaxExtendTerm()) {
            throw new BusinessValidationException(ErrorEnum.TIME_AMOUNT);
        }
    }
}
