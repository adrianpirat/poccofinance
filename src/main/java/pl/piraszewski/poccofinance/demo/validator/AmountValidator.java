package pl.piraszewski.poccofinance.demo.validator;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;
import pl.piraszewski.poccofinance.demo.api.model.ErrorEnum;
import pl.piraszewski.poccofinance.demo.dao.AmountDao;
import pl.piraszewski.poccofinance.demo.exception.BusinessValidationException;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Component
public class AmountValidator {

    private AmountDao amountDao;

    public AmountValidator(AmountDao amountDao) {
        this.amountDao = amountDao;
    }

    public void validator(final BigDecimal amount, BigDecimal interest) throws BusinessValidationException {
        if (amount == null) {
            throw new BusinessValidationException(ErrorEnum.NULL_AMOUNT);
        }
        if (interest == null) {
            throw new BusinessValidationException(ErrorEnum.NULL_INTEREST);
        }
        maxValidator(amount);
        minValidator(amount);
        timeValidator(amount);
        validatorInterest(interest);
    }

    private void validatorInterest(BigDecimal interest) throws BusinessValidationException {
        if (BigDecimal.TEN.compareTo(interest) != 0) {
            throw new BusinessValidationException(ErrorEnum.VALUE_INTEREST);
        }
    }

    private void maxValidator(final BigDecimal amount) throws BusinessValidationException {
        if (amount.compareTo(amountDao.getMax()) > 0) {
            throw new BusinessValidationException(ErrorEnum.MAX_AMOUNT);
        }
    }

    private void minValidator(final BigDecimal amount) throws BusinessValidationException {
        if (amount.compareTo(amountDao.getMin()) <= 0) {
            throw new BusinessValidationException(ErrorEnum.MIN_AMOUNT);
        }
    }

    private void timeValidator(BigDecimal amount) throws BusinessValidationException {
        try {
            if (amount.compareTo(amountDao.getMax()) == 0 && isTimeBetweenTwoTime("00:00:00", "06:00:00")) {
                throw new BusinessValidationException(ErrorEnum.TIME_AMOUNT);
            }
        } catch (ParseException e) {
            throw new IllegalArgumentException("Not a valid time, expecting HH:MM:SS format");
        }
    }

    private static boolean isTimeBetweenTwoTime(String initialTime, String finalTime) throws ParseException {
        String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
        if (initialTime.matches(reg) && finalTime.matches(reg)) {
            boolean valid = false;
            //Start Time
            java.util.Date inTime = new SimpleDateFormat("HH:mm:ss").parse(initialTime);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(inTime);

            //End Time
            java.util.Date finTime = new SimpleDateFormat("HH:mm:ss").parse(finalTime);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(finTime);


            //Current Time
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(DateTime.now().toDate());

            java.util.Date actualTime = calendar3.getTime();
            if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0)
                    && actualTime.before(calendar2.getTime())) {
                valid = true;
            }
            return valid;
        } else {
            throw new IllegalArgumentException("Not a valid time, expecting HH:MM:SS format");
        }

    }


}
