package pl.piraszewski.poccofinance.demo.api.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Data
@Getter
@Setter
public class LoanExtendRequest extends LoanRequest {

    private Long extendDays;
}
