package pl.piraszewski.poccofinance.demo.api.model;

public enum StatusEnum {

    ACCEPTED, DECLINED
}
