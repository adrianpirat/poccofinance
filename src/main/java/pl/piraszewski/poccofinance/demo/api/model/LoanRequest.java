package pl.piraszewski.poccofinance.demo.api.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Data
@Getter
@Setter
public class LoanRequest {

    private BigDecimal loan;

    private Long loanDays;

    private BigDecimal interest;
}
