package pl.piraszewski.poccofinance.demo.api.model;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Setter
@Getter
public class LoanResponse {

    private StatusEnum statusEnum;

    private String reasonForRejection;

}
