package pl.piraszewski.poccofinance.demo.api.model;

public enum ErrorEnum {

    NULL_LOAN_REQUEST("Empty request"),
    NULL_AMOUNT("Empty amount value"),
    NULL_INTEREST("Empty interest value"),
    MAX_AMOUNT("Amount value is to hight"),
    MIN_AMOUNT("Amount value is to low"),
    TIME_AMOUNT("Loan time error"),
    VALUE_INTEREST("Interest value is not correct"),
    EXTEND_VALUE("Extend value is not correct"),

    EMPTY_LOAN("Empty input data"),

    ;


    private String msg;

    ErrorEnum(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
