package pl.piraszewski.poccofinance.demo.exception;

import pl.piraszewski.poccofinance.demo.api.model.ErrorEnum;

public class BusinessValidationException extends Exception {

    private BusinessValidationException() {
        super();
    }

    public BusinessValidationException(ErrorEnum message) {
        super(message.getMsg());
    }
}
