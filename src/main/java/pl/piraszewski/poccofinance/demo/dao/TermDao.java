package pl.piraszewski.poccofinance.demo.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
@Getter
@Setter
public class TermDao {

    @Value("${poccofinance.term.max}")
    private Long maxTerm;

    @Value("${poccofinance.term.maxExtend}")
    private Long maxExtendTerm;


}
