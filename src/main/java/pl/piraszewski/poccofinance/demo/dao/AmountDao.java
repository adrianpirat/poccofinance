package pl.piraszewski.poccofinance.demo.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
@Getter
@Setter
public class AmountDao {

    @Value("${poccofinance.amount.min}")
    private BigDecimal min;

    @Value("${poccofinance.amount.max}")
    private BigDecimal max;


}
